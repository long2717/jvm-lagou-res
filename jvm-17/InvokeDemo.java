interface I {
    default void infMethod() { }

    void inf();
}

abstract class Abs {
    abstract void abs();
}

public class InvokeDemo extends Abs implements I {


    static void staticMethod() { }

    private void privateMethod() { }

    public void publicMethod() { }

    @Override
    public void inf() { }

    @Override
    void abs() { }

    public static void main(String[] args) throws Exception{
        InvokeDemo demo = new InvokeDemo();

        InvokeDemo.staticMethod();
        demo.abs();
        ((Abs) demo).abs();
        demo.inf();
        ((I) demo).inf();
        demo.privateMethod();
        demo.publicMethod();
        demo.infMethod();
        ((I) demo).infMethod();


        Thread.sleep(Integer.MAX_VALUE);
    }
}
